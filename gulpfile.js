var
    browserSync = require( 'browser-sync' ),
    del = require( 'del' ),
    exec = require('child_process').exec,
    gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    newer = require( 'gulp-newer' ),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    config = require( './gulp-config' )
    ;
    
    console.log( config );

/** Optimize **/
function optimize( callback, environment ){
    var build_path = config.theme.path + config.source.path + config.source.scripts.path + config.source.scripts.build.path + config.source.scripts.build[ environment ]
        ;
    console.log('---------------------------------');
    console.log(' Optimizing for "' + environment + '"' );
    console.log(' Using: ' + build_path );
    console.log('---------------------------------');

    // https://www.npmjs.com/package/gulp-exec
    del([ config.theme.path + config.asset.path + config.asset.scripts.path + '/**/*' ]);
    exec( 'r.js -o ' + build_path, function (err, stdout, stderr) {
        console.log( stdout );
        console.log( stderr );
        callback( err );
    });
}

gulp.task('optimize:development', function ( callback ) {
    optimize( callback, 'development' );
});

gulp.task('optimize:production', function ( callback ) {
    optimize( callback, 'production' );
});

/** BrowserSync **/
gulp.task('browsersync', function () {

    console.log('---------------------------------');
    console.log(' Watching path: ' + config.theme.path + config.asset.path + config.asset.css.path + '/**/*.css' );
    console.log('---------------------------------');

    browserSync({
        proxy: 'localhost:3001',
        files: config.theme.path + config.asset.path + config.asset.css.path + '/**/*.css',
        port: 8082
    });

});

/** Styles **/
gulp.task('sass', function() {
    del([ config.theme.path + config.asset.path + config.asset.css.path + '/**/*' ]);
    return gulp.src( config.theme.path + config.source.path + config.source.sass.path + '/' + config.source.sass.filename )
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe( gulp.dest( config.theme.path + config.asset.path + config.asset.css.path ) );

});

/** Images **/
gulp.task('images', function() {
    // del([ config.theme.path + config.asset.path + config.asset.images.path + '/**/*' ]);
    return gulp.src( config.theme.path + config.source.path + config.source.images.path + '/' + config.source.images.files )
        .pipe( newer( config.theme.path + config.asset.path + config.asset.images.path ) )
        .pipe( imagemin() )
        .pipe( gulp.dest( config.theme.path + config.asset.path + config.asset.images.path ) );

});

gulp.task( 'watch', function() {

    console.log('---------------------------------');
    console.log(' Watching SCSS: ' + config.theme.path + config.source.path + config.source.sass.path );
    console.log('---------------------------------');

    // if any sass files change(in this folder, [compile sass])
    gulp.watch( config.theme.path + config.source.path + config.source.sass.path + '/**/*.scss', ['sass'] );

    console.log('---------------------------------');
    console.log(' Watching Images: ' + config.theme.path + config.source.path + config.source.images.path );
    console.log('---------------------------------');
    gulp.watch( config.theme.path + config.source.path + config.source.images.path + config.source.images.files , ['images'] );

    console.log('---------------------------------');
    console.log(' Watching Scripts: ' + config.theme.path + config.source.path + config.source.scripts.path );
    console.log('---------------------------------');
    gulp.watch( config.theme.path + config.source.path + config.source.scripts.path +  config.source.scripts.files , [ 'optimize:development' ] );

});

gulp.task( 'default', [ 'images', 'sass', 'optimize:development', 'watch'] );
gulp.task( 'dev', [ 'default' ] );
gulp.task( 'build', [ 'images', 'sass', 'optimize:production' ] );
gulp.task( 'prod', [ 'build' ] );