                         _           _          
      _______ ___ ___ __(_)______   (_)__       
     / __/ -_) _ `/ // / / __/ -_) / (_-<       
    /_/  \__/\_, /\_,_/_/_/  \__/_/ /___/       
       __     /_/_ __          |___/     __     
      / /  ___  (_) /__ _______  / /__ _/ /____ 
     / _ \/ _ \/ / / -_) __/ _ \/ / _ `/ __/ -_)
    /_.__/\___/_/_/\__/_/ / .__/_/\_,_/\__/\__/ 
                         /_/                    

## Instructions

Run `$ npm install` 

## Gulp

`$ gulp` | `$ gulp dev`: Runs Image, Style, and Script processes once, the watches specified folders. The `r.js` optimization is uncompressed.
`$ gulp build` | `$ gulp prod`: Runs Image, Style, and Script processes (only once). The `r.js` optimization is uglified.

## Todo

* Figure out FontAwesome | TypeKit integration
* Install Rollbar
* Flesh out the SCSS variables a bit
* Identify/define/document candidate paths/configurations for folding into a `project init` bash script (think: `npm init`)
* Create Dev/Prod versions of the Image watcher task, such that Prod DOES `del([ config.theme.path + config.asset.path + config.asset.images.path + '/**/*' ])` (at least once, the first time it runs?)
* Reroute the SCSS `_variables.scss` mess to abstract out having to edit the `vendor/bootstrap` file(s)
* Extract the "viewport router" into its own module

## Notes

* The RequireJS `../source/scripts/main.js` and `../source/scripts/vendor/requirejs/build/*.js` configurations are particularly fussy about paths.
