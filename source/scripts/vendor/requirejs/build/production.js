({
    paths: {
        requirelib: "vendor/requirejs/require",
        jquery: "empty:"
    },
    include: 'requirelib',
    mainConfigFile: '../../../main.js',
    name: "main",
    optimize: 'uglify',
    findNestedDependencies: true,
    out: "../../../../../dist/js/main-built.js"
})