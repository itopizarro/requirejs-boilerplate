/*
    This file acts as a configuration and kickoff point for the script environment
*/
'use strict';

// Register extant jQuery library
// http://www.manuel-strehl.de/dev/load_jquery_before_requirejs.en.html
define('jquery', [], function() {
    return jQuery;
});

require.config({
    baseUrl: "./",
    paths: {
        "app": "app",
        "json": "vendor/requirejs/plugins/json",
        "text": "vendor/requirejs/plugins/text",
        "utilities": "utilities/utilities"
    }
});

require(
    [
        "app",
        "jquery",
        "json",
        "text"
    ],
    function ( app, $ ) {
        console.log( 'RequireJS!', arguments );
        "use strict";
        app.init();
    }
);