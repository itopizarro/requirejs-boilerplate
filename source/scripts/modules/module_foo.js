define([
    "jquery",
    "utilities"
    ], function( $ ) {

    'use strict';

    var app = window.app || {},
        Cache = app.Cache || {},

        init,
        REFERENCE = 'foo-module'
        ;

    Cache.$document = Cache.$document || $( document );

    /* I */
    init = function (){
        log( 'init', REFERENCE );
    };
    
    return {
        init: init
    };

} );