/*
    This file acts as the kickoff point for the application's scripts/component modules.
*/
define([
    "jquery",
    "utilities",
    "json!models/model_responsive.json" // ,
    // "vendor"
    ], function( $ , Utilities, Responsive/*, Vendor */ ) {
        "use strict";

        log( 'app.js' );
        // Register window.app & any other globally available stuff
        window.app = window.app || {}; 
        log( 'app', window.app );
        window.app.Utilities = Utilities;
        window.app.Responsive = Responsive;
        window.app.Metrics = new window.Metrics(); // for storing method performance data

        var 
            app = window.app,
            Cache = {},
            Metrics = app.Metrics,

            // methods
            build_element_cache,
            init,
            onResizeWindow,
            publish_traffic_events,
            viewport_router,

            // "constants"
            ARRAY_VIEWPORT_STATES, // gets populated by 'viewport_router'
            EVENT_VIEWPORT_SLUG = 'viewport.change.', // event nomenclature = 'viewport.change.[ enter | leave ].[ 'mobile', 'tablet', 'desktop' ]'
            REFERENCE = 'application'
            ;


        /* B */
        build_element_cache = function (){
            // cache element reference(s)

            // Basics
            Cache.$document = $( document );
            Cache.$window = $( window );
            Cache.$html = $( 'html' );
            Cache.$body = $( 'body' );
            Cache.$safeDOMReference = $( 'html, body' );
            
            app.Cache = Cache;
        };
    
        /* I */
        init = function (){
            // cache DOM refernces
            build_element_cache();
            viewport_router();
            
            // load module(s)
            // Page Header
            if ( 'test' ){
                require( [ 'modules/module_foo' ], function ( module_foo ){
                    module_foo.init();
                } );
            }
    
            Metrics.complete();
        };
    
        /* O */    
        onResizeWindow = function ( event ){
            var window_width = Cache.$window.width(),
                window_height = Cache.$window.height()
                ;

            Responsive.viewport.width = window_width;
            Responsive.viewport.height = window_height;

            if( window_width < Responsive.breakpoint.tablet ){
                publish_traffic_events( 0 );
            } else if ( window_width < Responsive.breakpoint.desktop ){
                publish_traffic_events( 1 );
            } else {
                publish_traffic_events( 2 );
            }
    
            Cache.$document.trigger( EVENT_VIEWPORT_SLUG + 'resize' );

        };
    
        /* P */
        publish_traffic_events = function ( index ){
            var ARRAY = ARRAY_VIEWPORT_STATES.slice()
                ;
            if ( Responsive.view != ARRAY[ index ] ){
                log( 'Viewport change to: ' + ARRAY[ index ] );
                Responsive.view = ARRAY[ index ];
                Cache.$document.trigger( EVENT_VIEWPORT_SLUG + 'enter.' + ARRAY[ index ] );
                ARRAY.splice( index, 1 );
    
                for ( var i = 0; i < ARRAY.length; i++ ){
                    Cache.$document.trigger( EVENT_VIEWPORT_SLUG + 'leave.' + ARRAY[ i ] );
                    log( ' ¬ ' + EVENT_VIEWPORT_SLUG + 'leave.' + ARRAY[ i ] );
                }
            }
            
        };
    
        /* V */
        viewport_router = function (){
            // Populate 'ARRAY_VIEWPORT_STATES' from the window.app.Responsive object
            ARRAY_VIEWPORT_STATES = [];
            Responsive.viewport = Responsive.viewport || {};

            for ( var key in Responsive.breakpoint ){
                if ( Responsive.breakpoint.hasOwnProperty( key ) ) {
                    ARRAY_VIEWPORT_STATES.push( key );
                } 
            }
            onResizeWindow(); // to catch the initial state
            Cache.$window.on( 'resize', onResizeWindow );
        };

        return {
            "init": init
        };
    } 
);