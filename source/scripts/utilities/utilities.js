define([
  "jquery",
  "utilities/debounce.js",
  "utilities/metrics.js",
  "utilities/polyfill.js",
  "utilities/safelog.js"
], function( $ ) {
  "use strict";

  log( 'utilities.js' );

  var Utilities = {};

  // Utility: Nano Template
  // https://github.com/trix/nano
  // usage:  window.app.Utilities.nano("<div>Foo {bar}!", { "bar": "baz" })
  // WARNING: watch those leading/trailing spaces in the {placeholder}
  Utilities.nano = function nano(template, data) {
    return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
      var keys = key.split("."), v = data[keys.shift()];
      for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
      return (typeof v !== "undefined" && v !== null) ? v : "";
    });
  };

  // Utility: Sort By
  // http://stackoverflow.com/a/6913821/5796134
  // Usage: [ { "foo": "1", "bar", "3" }, { "foo": "2", "bar", "2" }, { "foo": "3", "bar", "1" } ].sort( sort_by( "bar", {name:'foo', primer: parseInt, reverse: true} ) );
  Utilities.sort_by = function() {
    var fields = [],
      n_fields = arguments.length,
      field,
      name,
      reverse,
      cmp,
      default_cmp,
      getCmpFunc;

    default_cmp = function(a, b) {
      if (a == b) return 0;
      return a < b ? -1 : 1;
    };

    getCmpFunc = function(primer, reverse) {
      var dfc = default_cmp, // closer in scope
        cmp = default_cmp;
      if (primer) {
        cmp = function(a, b) {
          return dfc(primer(a), primer(b));
        };
      }
      if (reverse) {
        return function(a, b) {
          return -1 * cmp(a, b);
        };
      }
      return cmp;
    };

    // preprocess sorting options
    for (var i = 0; i < n_fields; i++) {
      field = arguments[i];
      if (typeof field === 'string') {
        name = field;
        cmp = default_cmp;
      }
      else {
        name = field.name;
        cmp = getCmpFunc(field.primer, field.reverse);
      }
      fields.push({
        name: name,
        cmp: cmp
      });
    }

    // final comparison function
    return function(A, B) {
      var a, b, name, result;
      for (var i = 0; i < n_fields; i++) {
        result = 0;
        field = fields[i];
        name = field.name;

        result = field.cmp(A[name], B[name]);
        if (result !== 0) break;
      }
      return result;
    };
  };

  // Utility: Scroll Helpers
  Utilities.scrollAnimate = function(element, offset, duration) {
    /**
     * `element` is expecting a [naked] DOM element reference
     * `number` is expecting a Number to use as an offset from the top of the window
     **/
    var $target = jQuery(element),
      _offset = (offset) ? offset : 0,
      _duration = (duration) ? duration : 0;

    if ($target.length != 1) {
      // log( 'Error @ window.app.Utilities.scrollAnimate: Cannot scroll to multiple elements' );
      return;
    }

    $('html, body').animate({
      "scrollTop": $target.offset().top - _offset
    }, _duration);

  };

  Utilities.scrollToAnchor = function(string, number) {
    /**
     * `string` is expecting a String from an element's `href` attribute OR the `location.hash` INCLUDING the "#"
     * `number` is expecting a Number to use as an offset from the top of the window
     * NOTE: The `string` could be a valid URL, a hash fragment, blank space, slash, or *anything* a person can type.
     **/
    var $target,
      offset = (number) ? number : 0;

    // Validate that the link we caught points to an actual element on the page
    if (string.charAt(0) == '#' && string.length > 1) {
      // Yes! Sort of!
      $target = jQuery('[ id="' + string.replace('#', '') + '" ]');
      if ($target.length === 0) {
        // Except: no!
        return;
      }
    }
    else {
      // Let the browser try to do what it was going to do, I wash my hands of it…
      window.location = string;
      return;
    }

    window.app.Utilities.scrollAnimate($target[0], offset, 500);

  };

  return Utilities;
});
