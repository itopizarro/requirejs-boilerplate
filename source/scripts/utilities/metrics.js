define( [], function (){
    window.Metrics = function (){
        this.started = this.stamp();
        this.completed = false;
    };
    window.Metrics.prototype.complete = function (){
        this.completed = this.stamp();
        this.calculate();
    };
    window.Metrics.prototype.calculate = function ( reference ){
        this.duration = ( this.completed - this.started );
    };
    window.Metrics.prototype.stamp = function (){
        return Date.now();
    };
});